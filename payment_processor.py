import requests
import json
from hashlib import sha256
from urllib.parse import urlencode, quote_plus
from flask import redirect


def error_message(mess):
    return mess


def sign_data(data, fields):
    secretKey = 'SecretKey01'
    sign_string = ':'.join(
        str(data[i]) for i in sorted(fields.values())
    ) + secretKey
    return sha256(sign_string.encode('utf-8')).hexdigest()


def preprocess_data(raw_data, base_data, fields, currency_code):
    form_data = {fields.get(k, k): v for (k, v) in raw_data.items()}  # copy
    form_data.update(base_data)
    form_data[fields.get('currency', 'currency')] = currency_code
    form_data['sign'] = sign_data(form_data, fields)
    return form_data


def process_eur(raw_data):
    url = 'https://pay.piastrix.com/en/pay'

    form_data = preprocess_data(
        raw_data,
        {'shop_id': 5, 'shop_order_id': 123456},
        {
            'amount': 'amount',
            'currency': 'currency',
            'shop_id': 'shop_id',
            'shop_order_id': 'shop_order_id',
        },
        978
    )
    params = '&'.join(
        k + '=' + str(v)
        for (k, v) in form_data.items()
    )
    return redirect(url + '?' + params)


def process_usd(raw_data):
    url = 'https://core.piastrix.com/bill/create'

    form_data = preprocess_data(
        raw_data,
        {'shop_id': 5, 'shop_order_id': 123456, 'payer_currency': 840},
        {
            'amount': 'shop_amount',
            'currency': 'shop_currency',
            'shop_id': 'shop_id',
            'shop_order_id': 'shop_order_id',
            '': 'payer_currency'  # because we need it to calculate sign
        },
        840,
    )

    resp = requests.post(
        url, data=json.dumps(form_data),
        headers={'content-type': 'application/json'}
    )
    resp_data = resp.json()
    if resp_data.get('error_code', 0):
        return error_message(resp_data['message'])

    form_data = resp_data['data']
    url = form_data.pop('url', None)
    params = '&'.join(
        k + '=' + str(v)
        for (k, v) in form_data.items()
    )
    return redirect(url + '?' + params)


def process_rub(raw_data):
    url = 'https://core.piastrix.com/invoice/create'

    form_data = preprocess_data(
        raw_data,
        {'shop_id': 5, 'shop_order_id': 123456, 'payway': 'payeer_rub'},
        {
            'amount': 'amount',
            'currency': 'currency',
            'shop_id': 'shop_id',
            'shop_order_id': 'shop_order_id',
            '': 'payway'  # because we need it to calculate sign
        },
        643,
    )

    resp = requests.post(
        url, data=json.dumps(form_data),
        headers={'content-type': 'application/json'}
    )
    resp_data = resp.json()
    if resp_data.get('error_code', 0):
        return error_message(resp_data['message'])

    form_data = resp_data['data']
    url = form_data.pop('url', None)
    params = '&'.join(
        k + '=' + str(v)
        for (k, v) in form_data['data'].items()
    )

    return redirect(url + '?' + params)


actions = {
    'eur': process_eur,
    'usd': process_usd,
    'rub': process_rub
}


def process_payment(raw_data):
    if 'currency' not in raw_data:
        return error_message('Currency value is missing')
    return actions[raw_data['currency']](raw_data)
