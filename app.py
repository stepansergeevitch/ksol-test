import time
from flask import Flask, render_template, request
import logging
from logging.handlers import RotatingFileHandler
from logging import info as log_operation, INFO

from payment_processor import process_payment

app = Flask(__name__)

handler = RotatingFileHandler('payments.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)


@app.route("/")
def hello():
    return render_template('main.html')


def log_payment(raw_data):
    app.logger.warning(
        "{} payment {} {} ({})".format(
            time.ctime(),
            raw_data.get('amount', '?'),
            raw_data.get('currency', '?'),
            raw_data.get('description', '?')
        )
    )


@app.route("/payment", methods=['POST'])
def accept_payment():
    log_payment(request.form)
    return process_payment(request.form)
